function errorResponseFancyHandler(err, req, res, next) {
  let statusCode = 500
  if (err.statusCode) statusCode = err.statusCode
  res.status(statusCode).json(responseErrorFancy(statusCode, err, req, res, next))
  return next(err, req, res, next)
}
const typeError = (statusCode) => {
  switch(statusCode) {
    case 500: return 'EXTERNAL_ERROR'
    case 403: return 'EXTERNAL_ERROR'
    case 404: return 'INTERNAL_ERROR'
    default: return 'ERROR'
  }
}
const responseErrorFancy = (statusCode, err, req, res) => {
  return ({
    xReqId: res.locals.xReqId,
    type: typeError(statusCode),
    category: '',
    error: {
      payload: {
        url: req.url,
        params: req.params
      },
      exception: err.message,
      statusCode,
    },
    contex: {
      xReqId: res.locals.xReqId
    }
  })
}

module.exports = errorResponseFancyHandler
